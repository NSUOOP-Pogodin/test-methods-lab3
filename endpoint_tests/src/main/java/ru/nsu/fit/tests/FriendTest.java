package ru.nsu.fit.tests;

import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.Screen.LoginScreen;
import ru.nsu.fit.tests.Screen.SearchScreen;
import ru.nsu.fit.tests.Screen.StartScreen;
import ru.nsu.fit.tests.Screen.UserScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by I on 25.12.2016.
 */
public class FriendTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }
    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
    @Test
    @Title("Add friend")
    @Description("Add friend via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Adding friend feature")
    public void addFriend() throws InterruptedException {
        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();
        LoginScreen loginScreen = startScreen.clickOnLogin();
        loginScreen.tryLogin("bukshev@gmail.com", "FBRosary70tyZ");

        Thread.sleep(1500);
        SearchScreen searchScreen = startScreen.search("Ruslan Pogodin");

        Thread.sleep(2500);
        UserScreen userScreen = searchScreen.clickOnFoundedHeader();

        Thread.sleep(1500);
        userScreen.clickOnAddFriend();

        Thread.sleep(2000);
        Assert.assertEquals(userScreen.getResultAdding(), "Ruslan Pogodin не ответил на последнее подмигивание.");
    }

    @Test
    @Title("Message")
    @Description("Message via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Sending message")
    public void message() throws InterruptedException {
        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();
        LoginScreen loginScreen = startScreen.clickOnLogin();
        loginScreen.tryLogin("bukshev@gmail.com", "FBRosary70tyZ");

        Thread.sleep(1500);
        SearchScreen searchScreen = startScreen.search("Ruslan Pogodin");

        Thread.sleep(4500);
        UserScreen userScreen = searchScreen.clickOnFoundedHeader();

        Thread.sleep(1500);
        userScreen.clickOnMessage("Free Yung Trappa!");

        Thread.sleep(500);
        Assert.assertEquals(userScreen.getResultMessaging(), "Ваше сообщение было успешно отправлено.");
    }
}

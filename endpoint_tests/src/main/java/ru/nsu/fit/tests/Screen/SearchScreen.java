package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

/**
 * Created by I on 25.12.2016.
 */
public class SearchScreen {
    private Browser browser;
    private final String SEARCH_TITLE_XPATH = "//*[@id=\"q\"]";

    public SearchScreen(Browser browser) {
        this.browser = browser;
    }

    public String getResultSearchTitle() {
        return browser.getElement(By.xpath(SEARCH_TITLE_XPATH)).getText();
    }

    public String getNotFoundMessage() {
        String messageNotFoundXPath = "//*[@id=\"js_62l\"]/a/span/span";
        return browser.getElement(By.xpath(messageNotFoundXPath)).getText();
    }

    public UserScreen clickOnFoundedHeader() {
        browser.click(By.className("_5d-5"));
        return new UserScreen(browser);
    }
}

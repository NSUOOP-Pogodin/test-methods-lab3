package ru.nsu.fit.tests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.Screen.LoginScreen;
import ru.nsu.fit.tests.Screen.StartScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class AcceptanceTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("SuccessForLogIn")
    @Description("Test login with success result via API")
    @Severity(SeverityLevel.BLOCKER)
    public void successForLogIn() throws InterruptedException {
        LoginScreen loginScreen = tryLoginWithStartPageRunning("bukshev@gmail.com", "FBRosary70tyZ");
        loginScreen.waitChangeUser();

        Assert.assertEquals(loginScreen.getCurrentUser(), "Ivan Bukshev");
    }

    @Test
    @Title("FailForLogInByPassword")
    @Description("Test login with fail result because of wrong login via UI API")
    @Severity(SeverityLevel.BLOCKER)
    public void failForLogInByLogin() throws InterruptedException {
        LoginScreen loginScreen = tryLoginWithStartPageRunning("bukshev@gmail.com", "FBRosay70tyz");
        Assert.assertEquals(browser.getElement(By.xpath("//*[@id=\"js_0\"]/div/div/div")).getText(), "Вы ввели неверный пароль. Забыли пароль?");
    }

    public LoginScreen tryLoginWithStartPageRunning(String login, String password) {
        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();
        LoginScreen loginScreen = startScreen.clickOnLogin();
        loginScreen.tryLogin(login, password);
        return loginScreen;
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}

package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;

/**
 * Created by I on 25.12.2016.
 */
public class StartScreen {
    private Browser browser;
    public StartScreen(Browser browser) {
        this.browser = browser;
    }

    public void openStartPage() {
        String startPageUrl = "https://www.facebook.com/";

        browser.openPage(startPageUrl);
        AllureUtils.saveImageAttach("facebook.com", browser.makeScreenshot());
        AllureUtils.saveTextLog("Page loaded");
    }
    public LoginScreen clickOnLogin() {
        return new LoginScreen(browser);
    }

    public SearchScreen search(String text) {
        String searchCommandXPath = "//*[@id=\"q\"]";
        String buttonSearchClass = "reDesignedBtn";
        browser.typeText(By.xpath(searchCommandXPath), text);
        browser.click(By.className(buttonSearchClass));
        return new SearchScreen(browser);
    }

}

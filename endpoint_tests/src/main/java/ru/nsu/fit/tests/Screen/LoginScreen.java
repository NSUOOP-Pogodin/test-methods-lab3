package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;

/**
 * Created by I on 25.12.2016.
 */
public class LoginScreen {
    private Browser browser;
    public LoginScreen(Browser browser) {
        this.browser = browser;
    }

    private void fillLoginPassword(String login, String password) {
        String loginXPath = "//tr[2]/td[1]/input";
        String passwordXPath = "//tr[2]/td[2]/input";
        browser.getElement(By.xpath(loginXPath)).sendKeys(login);
        browser.getElement(By.xpath(passwordXPath)).sendKeys(password);
        AllureUtils.saveImageAttach("facebook.com ввод данных", browser.makeScreenshot());
        AllureUtils.saveTextLog("Data entered");
    }

    public void tryLogin(String login, String password) {
        fillLoginPassword(login, password);
        String enterButtonXPath = "//tr[2]/td[3]/label/input";
        browser.click(By.xpath(enterButtonXPath));

        AllureUtils.saveImageAttach("facebook.com Вход", browser.makeScreenshot());
        AllureUtils.saveTextLog("Вход pushed");
    }

    public String getCurrentUser() {
        String currentUserXPath = "//*[@id=\"pagelet_welcome_box\"]/ul/li/div/div/a";
        return browser.getElement(By.xpath(currentUserXPath)).getText();
    }
    public void waitChangeUser() {
        String currentUserXPath = "//*[@id=\"pagelet_welcome_box\"]/ul/li/div/div/a";
        browser.waitForElement(By.xpath(currentUserXPath));
    }
    public String getErrorLoginMessage() {
        String errorClassName = "auth__message-error";
        return browser.getElement(By.className(errorClassName)).getText();
    }
}

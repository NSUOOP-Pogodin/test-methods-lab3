package ru.nsu.fit.tests.Screen;

import com.sun.jna.platform.win32.Netapi32Util;
import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

/**
 * Created by I on 25.12.2016.
 */
public class UserScreen {
    private Browser browser;

    public UserScreen(Browser browser) {
        this.browser = browser;
    }

    public void clickOnAddFriend() {
        browser.click(By.cssSelector("*[class^='_42ft _4jy0 _1yzl _p _4jy4 _517h _51sy']"));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        browser.click(By.cssSelector("*[class^='_54ni __MenuItem']"));
    }

    public void clickOnMessage(String message) {
        browser.click(By.cssSelector("*[class^='fsl fcg']"));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String searchCommandXPath = "*[class^='uiTextareaNoResize uiTextareaAutogrow _2oj']";
        String buttonSearchXPath = "*[class^='_42ft _4jy0 layerConfirm _2ok uiOverlayButton _4jy4 _4jy1 selected _51sy']";
        browser.typeText(By.className("_2oj"), message);
        browser.click(By.cssSelector(buttonSearchXPath));
    }

    public String getResultAdding() {
        return browser.getElement(By.cssSelector("*[class^='_4bl9 mrm']")).getText();
    }
    public String getResultMessaging() {
        return browser.getElement(By.cssSelector("*[class^='_4-i2 _pig _50f4']")).getText();
    }
}
